//package com.leads.crawler.storage;

package org.se.mlMapReduce;

import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

//import com.leads.crawler.DispatchMemory;

//public class RedisDispatchMemory implements DispatchMemory {

public class RedisDispatchMemory {
	private Jedis _client = null;
	private JedisPool _jedisPool;
	
//	public RedisDispatchMemory(String hostname, int port){
//		
//		_client = new Jedis(hostname,port);
//		_jedisPool = new JedisPool(hostname, port);
//	}
	
	public void init(String hostname, int port) {
		_client = new Jedis(hostname , port);
		_jedisPool = new JedisPool (hostname , port);
	}
	

	public boolean exists(String key) {
		return _client.exists(key);
	}
	

	public void put(String key) {
		_client.set(key, "something");
	}
	

	public void shutdown() {
		_client.shutdown();
	}
	

	public Set<String> listKey(String key) {
		Set<String> result = null;
		Jedis jedis = _jedisPool.getResource();
		try {
			result = jedis.keys(key);

		}catch (Exception e) {
			result = null;
		}finally {
			_jedisPool.returnResource(jedis);
		}
		//Clear data in the Redis database after getting urls
		//jedis.flushDB();
		return result;
	}


	public void cleardb() {
		Jedis jedis = _jedisPool.getResource();
		try{
			jedis.flushDB();
		}catch (Exception e){
			
		}finally{
			_jedisPool.returnResource(jedis);
		}
		
	}

	public void del(String key) {
		_client.del(key);
		
		
	}
	

}