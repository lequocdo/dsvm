package org.se.mlMapReduce;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Set;
import java.util.StringTokenizer;

public class ModelFileFromReduce {
	public static void main(String[] args) throws Exception {

		String path = "model";									//model filename
		String modelIP;											//RedisDB model IP
		int modelPort;											//RedisDB model port
		String SVsIP;											//RedisDB SVs IP
		int SVsPort;											//RedisDB SVs port

		FileWriter model = new FileWriter(path);

		BufferedWriter buffer=new BufferedWriter (model);


		//Reading configuration file to get IPs and ports of RedisDBs
		FileReader configfile = new FileReader(ModelFileFromReduce.class.getResource("/conf/redisDBconfig.cfg").getPath());
		BufferedReader buf  = new BufferedReader(configfile);

		//Need to use temp_port because port is an integer
		String temp_port;

		//Avoid instruction comment
		buf.readLine();
		//Avoid comment
		buf.readLine();
		//Getting RedisDB model IP
		modelIP = buf.readLine();
		modelIP = modelIP.replace("IP=", "");


		//Getting RedisDB model Port
		temp_port = buf.readLine();
		temp_port = temp_port.replace("Port=", "");
		modelPort = Integer.parseInt(temp_port);

		//Avoid comment
		buf.readLine();
		//Getting RedisDB SVs IP
		SVsIP = buf.readLine();
		SVsIP = SVsIP.replace("IP=", "");

		//Getting RedisDB SVs Port
		temp_port = buf.readLine();
		temp_port = temp_port.replace("Port=", "");
		SVsPort = Integer.parseInt(temp_port);

		buf.close();

		RedisDispatchMemory redis_model = new RedisDispatchMemory();
		redis_model.init(modelIP, modelPort);			

		//Getting parameters from RedisDB
		Set<String> temp = redis_model.listKey("parameters*");

		String temp1 = temp.toString();
		temp1 = temp1.replace("parameters", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");

		StringTokenizer st = new StringTokenizer(temp1,";");

		//Skipping parameters not needed in model file
		//skip probability
		st.nextToken();
		//skip gamma
		st.nextToken();
		//skip nu
		st.nextToken();
		//skip C
		st.nextToken();

		//Getting svm_type from RedisDB
		Integer tmp=Integer.parseInt(st.nextToken().toString());

		//Switch from numeric value to string value (To make model file more understandable)
		switch(tmp){

		case 0: 
			buffer.write("svm_type c_svc\n");
			break;
		case 1:
			buffer.write("svm_type nu_svc\n");						//not used, can be useful for future implementations
			break;
		default:
			buffer.write("svm_type c_svc\n");						//not used, can be useful for future implementations (default value for LibSVM is c_svc)
		};

		//Getting kernel type from RedisDB
		tmp=Integer.parseInt(st.nextToken().toString());


		//Switch from numeric value to string value (To make model file more understandable)
		switch(tmp){

		case 0: 
			buffer.write("kernel_type linear\n");
			break;
		case 1:
			buffer.write("kernel_type polynomial\n");				//not used, can be useful for future implementations
			break;
		case 2:
			buffer.write("kernel_type radial basis function\n");	//not used, can be useful for future implementations
		default:
			buffer.write("kernel_type radial basis function\n");	//not used, can be useful for future implementations (default value used by LibSVM is RBF)
		};


		//Getting number of class
		temp = redis_model.listKey("nr_class*");
		temp1 = temp.toString();
		temp1 = temp1.replace("nr_class", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");

		buffer.write("nr_class "+temp1+"\n");


		//Getting total number of Support Vectors
		temp = redis_model.listKey("numSV*");
		temp1 = temp.toString();
		temp1 = temp1.replace("numSV", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");
		buffer.write("total_sv "+temp1+"\n");


		//Getting rho (b) values
		temp = redis_model.listKey("rho*");
		temp1 = temp.toString();
		temp1 = temp1.replace("rho", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");
		temp1 = temp1.replace(";", " ");
		buffer.write("rho "+temp1+"\n");


		//Getting labels
		temp = redis_model.listKey("labels*");
		temp1 = temp.toString();
		temp1 = temp1.replace("labels", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");
		temp1 = temp1.replace(";", " ");
		buffer.write("label "+temp1+"\n");


		//Getting number of support vectors for each class
		temp = redis_model.listKey("nSVs*");
		temp1 = temp.toString();
		temp1 = temp1.replace("nSVs", "");
		temp1 = temp1.replace("[", "");
		temp1 = temp1.replace("]", "");
		temp1 = temp1.replace(";", " ");
		buffer.write("nr_sv "+temp1+"\n");

		buffer.write("SV\n");


		//Getting support vectors
		RedisDispatchMemory redis_SVs = new RedisDispatchMemory();
		redis_SVs.init(SVsIP, SVsPort);		
		Set<String> tempSV = redis_SVs.listKey("NO_IT*");
		String tempSV1 = tempSV.toString();
		tempSV1 = tempSV1.replace("NO_IT @ ", "");
		tempSV1 = tempSV1.replace("NO_IT", "");
		tempSV1 = tempSV1.replace("[", "");
		tempSV1 = tempSV1.replace("]", "");
		st = new StringTokenizer(tempSV1,";");
		while(st.hasMoreTokens()){
			buffer.write(st.nextToken().toString()+"\n");
		}
		buffer.flush();
		buffer.close();
	}
}
