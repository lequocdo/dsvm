package org.se.mlMapReduce;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SVMClassifying {
	static Vector<Double> vy = new Vector<Double>();			//label vector
	static Vector<svm_node[]> vx = new Vector<svm_node[]>();	//nodes vector
	static svm_problem prob = new svm_problem();				//problem variable
	static svm_model model = new svm_model() ;					//model variable
	static svm_parameter parameters = new svm_parameter(); 		//parameters variable
	static String modelIP;											//RedisDB model IP
	static int modelPort;											//RedisDB model port
	static String SVsIP;											//RedisDB SVs IP
	static int SVsPort;												//RedisDB SVs port


	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		Logger maplog = LoggerFactory.getLogger(Map.class);

		//Using setup function to construct the model object to test the input vectors
		protected void setup(Context context) throws IOException
		{


			//Reading configuration file to get IPs and ports of RedisDBs
			FileReader configfile = new FileReader(SVMClassifying.class.getResource("/redisDBconfig.cfg").getPath());

			BufferedReader b  = new BufferedReader(configfile);

			//Need to use temp_port because port is an integer
			String temp_port;

			//Avoid instruction comment
			b.readLine();
			//Avoid comment
			b.readLine();
			//Getting RedisDB model IP
			modelIP = b.readLine();
			modelIP = modelIP.replace("IP=", "");


			//Getting RedisDB model Port
			temp_port = b.readLine();
			temp_port = temp_port.replace("Port=", "");
			modelPort = Integer.parseInt(temp_port);

			//Avoid comment
			b.readLine();
			//Getting RedisDB SVs IP
			SVsIP = b.readLine();
			SVsIP = SVsIP.replace("IP=", "");

			//Getting RedisDB SVs Port
			temp_port = b.readLine();
			temp_port = temp_port.replace("Port=", "");
			SVsPort = Integer.parseInt(temp_port);

			b.close();


			//Connection to Redis DB where model's parameters are stored
			RedisDispatchMemory redis_model = new RedisDispatchMemory();
			redis_model.init(modelIP, modelPort);			

			//Getting parameters from Redis
			Set<String> temp = redis_model.listKey("parameters*");
			String temp1 = temp.toString();

			//Adapting parameters to right format for parsing
			temp1 = temp1.replace("parameters", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");


			StringTokenizer st = new StringTokenizer(temp1,";");

			//Have to use Integer class because without it I can't use parseInt function
			Integer tmp;
			Double tmp_doub;

			tmp=Integer.parseInt(st.nextToken().toString());
			//Setting parameters values
			parameters.probability = tmp;	
			tmp_doub=Double.parseDouble(st.nextToken().toString());
			parameters.gamma = tmp_doub;
			tmp_doub=Double.parseDouble(st.nextToken().toString());
			parameters.nu = tmp_doub;
			tmp_doub=Double.parseDouble(st.nextToken().toString());
			parameters.C = tmp;
			tmp=Integer.parseInt(st.nextToken().toString());
			parameters.svm_type = tmp;
			tmp=Integer.parseInt(st.nextToken().toString());
			parameters.kernel_type = tmp;
			tmp_doub=Double.parseDouble(st.nextToken().toString());
			parameters.cache_size = tmp_doub;
			tmp_doub=Double.parseDouble(st.nextToken().toString());
			parameters.eps = tmp;

			//Assigning parameters object to "param" attribute of model object
			model.param = parameters;

			//Getting number of classes from Redis DB
			temp = redis_model.listKey("nr_class*");
			temp1 = temp.toString();

			//Parsing
			temp1 = temp1.replace("nr_class", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");
			tmp=Integer.parseInt(st.nextToken().toString());


			//Assigning the number of classes to model attribute
			model.nr_class = tmp;


			//Getting total number of support vectors 
			temp = redis_model.listKey("numSV*");
			temp1 = temp.toString();
			temp1 = temp1.replace("numSV", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");
			tmp=Integer.parseInt(st.nextToken().toString());

			//Number of SVs
			model.l = tmp;


			//Getting b values
			temp = redis_model.listKey("rho*");
			temp1 = temp.toString();
			temp1 = temp1.replace("rho", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");

			model.rho = new double[model.nr_class*(model.nr_class-1)/2];
			for (int i =0;i<model.nr_class*(model.nr_class-1)/2;i++){
				tmp_doub=Double.parseDouble(st.nextToken().toString());
				model.rho[i] = tmp_doub;				
			}

			//Pairwise probability 
			temp = redis_model.listKey("probA*");
			temp1 = temp.toString();
			temp1 = temp1.replace("probA", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");			

			model.probA = new double[model.nr_class*(model.nr_class-1)/2];
			for (int i =0;i<model.nr_class*(model.nr_class-1)/2;i++){
				tmp_doub=Double.parseDouble(st.nextToken().toString());
				model.probA[i] = tmp_doub;				
			}


			temp = redis_model.listKey("probB*");
			temp1 = temp.toString();
			temp1 = temp1.replace("probB", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");

			model.probB = new double[model.nr_class*(model.nr_class-1)/2];
			for (int i =0;i<model.nr_class*(model.nr_class-1)/2;i++){
				tmp_doub=Double.parseDouble(st.nextToken().toString());
				model.probB[i] = tmp_doub;				
			}


			temp = redis_model.listKey("labels*");
			temp1 = temp.toString();
			temp1 = temp1.replace("labels", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");

			model.label = new int[model.nr_class]; 

			for (int i=0; i<model.nr_class;i++){
				tmp=Integer.parseInt(st.nextToken().toString());
				model.label[i]=tmp;
			}


			//Number of support vectors of each class
			temp = redis_model.listKey("nSVs*");
			temp1 = temp.toString();
			temp1 = temp1.replace("nSVs", "");
			temp1 = temp1.replace("[", "");
			temp1 = temp1.replace("]", "");
			st = new StringTokenizer(temp1,";");
			model.nSV = new int [model.nr_class];

			for (int i=0; i<model.nr_class-1;i++){
				tmp=Integer.parseInt(st.nextToken().toString());

				model.nSV[i]=tmp;

			}


			//Getting support vectors from Redis DB
			RedisDispatchMemory redis_SVs = new RedisDispatchMemory();
			redis_SVs.init(SVsIP, SVsPort);			


			Set<String> tempSV = redis_SVs.listKey("NO_IT*");
			String tempSV1 = tempSV.toString();
			tempSV1 = tempSV1.replace("NO_IT", "");
			tempSV1 = tempSV1.replace("[", "");
			tempSV1 = tempSV1.replace("]", "");
			st = new StringTokenizer(tempSV1,";");
			int i=0;
			model.sv_coef = new double[model.nr_class-1][model.l];
			model.SV = new svm_node[model.l][];

			//Constructing Support Vectors object
			while (st.hasMoreTokens()){
				String SV = st.nextToken().toString();

				StringTokenizer st2 = new StringTokenizer (SV, " :");

				//Support Vectors coefficients
				for(int k=0;k<model.nr_class-1;k++){
					String tmmp=st2.nextToken().toString();
					model.sv_coef[k][i] = atof(tmmp);

				}	
				int numToken = st2.countTokens()/2;



				model.SV[i] = new svm_node[numToken];

				//SV index and value for each attribute/feature
				for(int j=0;j<numToken;j++)
				{
					model.SV[i][j] = new svm_node();
					model.SV[i][j].index = Integer.parseInt(st2.nextToken());
					model.SV[i][j].value = Double.valueOf(st2.nextToken()).doubleValue();
				}


				i++;
			}



		}






		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

			//Getting input vector
			String line = value.toString();
			String vector = new String();




			//Dividing in tokens I can take separately labels and attributes
			StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

			st.nextToken(); 	//avoiding the label

			//Have to do /2 because with tokenizer I take, everytime, also the feature index
			int m = st.countTokens()/2;

			//Constructing the the node object and the vector string (The string will be printed in the output file along with the classification result)
			svm_node[] x = new svm_node[m];
			for(int j=0;j<m;j++)
			{
				x[j] = new svm_node();
				x[j].index = Integer.parseInt(st.nextToken());
				x[j].value = atof(st.nextToken());
				vector += " "+x[j].index;
				vector += ":"+x[j].value;


			}


			//Classifying function
			Double result = svm.svm_predict(model, x);

			context.write(new Text(result.toString()),new Text (vector));

		}



		@Override
		public void cleanup(Context context) throws IOException, InterruptedException
		{


		}

	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {

		Logger reducelog = LoggerFactory.getLogger(Reduce.class);


		public void reduce(Text key, Iterable<Text> values, Context context) 
				throws IOException, InterruptedException {



		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "SVMClassifier");
		job.setJarByClass(SVMClassifying.class);
		job.setNumReduceTasks(0);				//Avoiding Reduce Task

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);



		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);



	}


	//Function to control if input is in correct format
	private static double atof(String s)
	{
		double d = Double.valueOf(s).doubleValue();
		if (Double.isNaN(d) || Double.isInfinite(d))
		{
			System.err.print("NaN or Infinity in input\n");
			System.exit(1);
		}
		return(d);
	}

}
