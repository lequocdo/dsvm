####### SE Group TU Dresden, Germany  ########

Distributed Suport Vector Machines.


There are 2 version of Training (./training):

1. MapReduce based SVM + Redis
2. MapNoReduce based SVM + Redis

One Classifying version for both version of Training (./classifying)


---------How To Use------------------

###Training:

Step 1: Put the traing dataset to HDFS
        $bin/hadoop dfs -D dfs.block.size=4718592 -put dataset.scale svmInput

Step 2: Complile the training code, copy the jar file to ~/hadoop, and copy redisDBconfig.cfg to ~/hadoop/conf

Step 3: Run Redis servers and modify redisDBconfig.cfg file

Step 4: Peform the training process:
        $./bin/hadoop jar MapAndReduce/build/jar/SVMTraining.jar svmInput svmOutput

Step 5: Repeat Step4 until get to convergent model (run $java -jar ModelFileFromRedis.jar to get the model and dump to a file for testing accuracy)


###Classifying

Step 1: Put the testing dataset to HDFS
        $bin/hadoop dfs -D dfs.block.size=2044724 -put svmtesting.scale svmTesting

Step 2: Perform the classifying process:
        $./bin/hadoop jar SVMTesting/build/jar/SVMTesting.jar svmTesting svmTestOutput
