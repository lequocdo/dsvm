package supportfromfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

public class SupportFromFile {
	
	static svm_problem prob;
	//svm_parameter param;

	public static void main(String []argvs){
				try {					
					  String path = new File("heart").getAbsolutePath();
					
			//Calling this function I can take the training File
			read_problem(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		svmTrain();
	}
	
	
	
	static void read_problem(String input_file) throws IOException
		{
			BufferedReader fp = new BufferedReader(new FileReader(input_file));
			Vector<Double> vy = new Vector<Double>();
			Vector<svm_node[]> vx = new Vector<svm_node[]>();
			int max_index = 0;

			while(true)
			{
				String line = fp.readLine();
				if(line == null) break;
				
				
				
				//Dividing in tokens I can take separately labels and attributes
				StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

				//Adding to the vector the previous token
				vy.addElement(atof(st.nextToken()));
				
				//Have to do /2 because with tokenizer I take, everytime, also the feature index
				int m = st.countTokens()/2;
				
				svm_node[] x = new svm_node[m];
				for(int j=0;j<m;j++)
				{
					x[j] = new svm_node();
					x[j].index = Integer.parseInt(st.nextToken());
					x[j].value = atof(st.nextToken());
				}
				if(m>0) max_index = Math.max(max_index, x[m-1].index);
				vx.addElement(x);
			}

			prob = new svm_problem();
			prob.l = vy.size();
			prob.x = new svm_node[prob.l][];
			for(int i=0;i<prob.l;i++)
				prob.x[i] = vx.elementAt(i);
			prob.y = new double[prob.l];
			for(int i=0;i<prob.l;i++)
				prob.y[i] = vy.elementAt(i);

			
			//commented because I decided a fixed gamma value, but it can be useful in the future if we want to do a control on gamma parameter 
			
			
		//	if(param.gamma == 0 && max_index > 0)
		//		param.gamma = 1.0/max_index;



			fp.close();
		}
		
		
		static private void svmTrain() {
		   

			//Setting parameters
		    svm_parameter param = new svm_parameter();
		    param.probability = 1;
		    param.gamma = 0.5;
		    param.nu = 0.5;
		    param.C = 1;
		    param.svm_type = svm_parameter.C_SVC;
		    param.kernel_type = svm_parameter.LINEAR;	//we decided to use Linear kernel       
		    param.cache_size = 20000;
		    param.eps = 0.001;      //LibSVM documentation say to use always this value of epsilon, only if we use nu-SVC we have to use 0.00001 as epsilon value

		    svm_model model = svm.svm_train(prob, param);
		    
		    //With the following code we can see which training instances are support vectors
		    int nr_sv = svm.svm_get_nr_sv(model);
		    int sv_index[]= new int [nr_sv];
		    svm.svm_get_sv_indices(model, sv_index);
		   
		    for (int i=0; i<nr_sv; i++){
				System.out.println("Instance "+sv_index[i]+ " is a support vector");
			
		    }
		    System.out.println("The total number of Support Vectors is: "+ nr_sv);
		    try {

		    	  String path = new File("heartModel.model").getAbsolutePath();
				//Saving the model obtained in heartModel.model file
				svm.svm_save_model(path, model);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    //Print founded SVs in console
		    printSVs(model);

		   
		}
		
		
		//Control if all the input are valid (Not a Number and Infinity input are not accepted
		private static double atof(String s)
		{
			double d = Double.valueOf(s).doubleValue();
			if (Double.isNaN(d) || Double.isInfinite(d))
			{
				System.err.print("NaN or Infinity in input\n");
				System.exit(1);
			}
			return(d);
		}
		
		private static void printSVs(svm_model model_){
			double[][] sv_coef = model_.sv_coef;
			svm_node[][] SV = model_.SV;
			int nr_class= svm.svm_get_nr_class(model_);
			int numSV= svm.svm_get_nr_sv(model_);
			

			for(int i=0;i<numSV;i++)
			{
				for(int j=0;j<nr_class-1;j++)
					System.out.print(sv_coef[j][i]+" ");

				svm_node[] p = SV[i];
			 	
					for(int j=0;j<p.length;j++)
						System.out.print(p[j].index+":"+p[j].value+" ");
				System.out.print("\n");
			}
		}

	
}
