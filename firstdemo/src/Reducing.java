package com.valerio;

import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


//My inputs are: The Key (IPAddress which is a Text object) and the SVs of the single machine (passed as a Text object)
//My outputs are: The Key and the final SVs which is the append 
public class Reducing extends Reducer<Text, Text, Text, Text> {
//public class Reducing extends Reducer<Object, Object, Object, Object> {  
	
	
	//creating the final String (will be the append of all SVs)
	  private String FinalSVs = new String();
	  
	 @Override
	 
	 public void reduce(Text key, Iterable<Text> values, Context context)  
	// public void reduce(Object key, Iterable<Object> values, Context context)
	 throws IOException, InterruptedException {
		 
	 
		 //Iterator of all strings received
	  Iterator<Text> it=values.iterator();
		// Iterator<Object> it=values.iterator();
		 StringBuffer sb = new StringBuffer();
	  while (it.hasNext()) {
	   
		  sb.append(it.next().toString());
	  }
	  
	  FinalSVs= sb.toString();
	  context.write(key, new Text(FinalSVs));
	  //context.write(key, FinalSVs);
	 }
	

}
