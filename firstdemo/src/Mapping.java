package com.valerio;

import java.io.IOException;
import java.net.InetAddress;
import java.util.StringTokenizer;
import java.util.Vector;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;



//My input will be Object and Text and the output will be Text,Text (IPAddress,SVs)
public class Mapping extends Mapper<Object, Text, Text, Text> {
//public class Mapping extends Mapper<Object, Text, Object, Object> {
		 @Override
		 public void map(Object key, Text value,
		   Context contex) throws IOException, InterruptedException {
			 
	//Setting the IP address, it will be the key
			 InetAddress IP = InetAddress.getLocalHost();
			 String IPAddress = IP.getHostAddress() ;
			 
			 svm_problem prob;
			 Vector<Double> vy = new Vector<Double>();
				Vector<svm_node[]> vx = new Vector<svm_node[]>();
				int max_index = 0;

				while(true)
				{
			
					String line = value.toString();
					if(line == null) break;
					
					
					
					//Dividing in tokens I can take separately labels and attributes
					StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

					//Adding to the vector the previous token
					vy.addElement(atof(st.nextToken()));
					
					//Have to do /2 because with tokenizer I take, everytime, also the feature index
					int m = st.countTokens()/2;
					
					svm_node[] x = new svm_node[m];
					for(int j=0;j<m;j++)
					{
						x[j] = new svm_node();
						x[j].index = Integer.parseInt(st.nextToken());
						x[j].value = atof(st.nextToken());
					}
					if(m>0) max_index = Math.max(max_index, x[m-1].index);
					vx.addElement(x);
				}

				prob = new svm_problem();
				prob.l = vy.size();
				prob.x = new svm_node[prob.l][];
				for(int i=0;i<prob.l;i++)
					prob.x[i] = vx.elementAt(i);
				prob.y = new double[prob.l];
				for(int i=0;i<prob.l;i++)
					prob.y[i] = vy.elementAt(i);

				
				//Specifying parameters
				svm_parameter param = new svm_parameter();
			    param.probability = 1;
			    param.gamma = 0.5;
			    param.nu = 0.5;
			    param.C = 1;
			    param.svm_type = svm_parameter.C_SVC;
			    param.kernel_type = svm_parameter.LINEAR;	//we decided to use Linear kernel       
			    param.cache_size = 20000;
			    param.eps = 0.001;      //LibSVM documentation say to use always this value of epsilon, only if we use nu-SVC we have to use 0.00001 as epsilon value

			    svm_model model = svm.svm_train(prob, param);
				
				
				
				
				//Constructing SVs
				
				double[][] sv_coef = model.sv_coef;
				svm_node[][] SV = model.SV;
				int nr_class= svm.svm_get_nr_class(model);
				int numSV= svm.svm_get_nr_sv(model);
				String SVs;
				StringBuffer sb = new StringBuffer(); 

				for(int i=0;i<numSV;i++)
				{
					for(int j=0;j<nr_class-1;j++)
				
					{	sb.append(sv_coef[j][i]);
						sb.append(" ");
					}			
						
						svm_node[] p = SV[i];

				 	
						for(int j=0;j<p.length;j++)
							{sb.append(p[j].index);
							sb.append(":");
							sb.append(p[j].value);
							sb.append(" ");

							}

							sb.append(";");
							
							/*
							SVs= sb.toString();
							contex.write(new Text(IPAddress), new Text(SVs));
							*/
						
				}
				
				SVs = sb.toString();
				
			 

					contex.write(new Text(IPAddress), new Text(SVs));
		   
		  
		 }
					
			
			//Control if all the input are valid (Not a Number and Infinity input are not accepted
			private static double atof(String s)
			{
				double d = Double.valueOf(s).doubleValue();
				if (Double.isNaN(d) || Double.isInfinite(d))
				{
					System.err.print("NaN or Infinity in input\n");
					System.exit(1);
				}
				return(d);
			}
			
		}
