package org.se.mlMapReduce;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URI;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SVMTraining {
	static Vector<Double> vy = new Vector<Double>();
	static Vector<svm_node[]> vx = new Vector<svm_node[]>();
	static svm_problem prob = new svm_problem();


	private final Logger LOG = LoggerFactory.getLogger(SVMTraining.class);


	protected void setup(Context context) throws IOException
	{

	}

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		Logger maplog = LoggerFactory.getLogger(Map.class);

		protected void setup(Context context) throws IOException
		{


			FileSystem fs = FileSystem.get(context.getConfiguration());
			Path out = new Path ("globalSV");
			if (fs.exists(out)){
				URI[] uriList = DistributedCache.getCacheFiles( context.getConfiguration() );

				for(int i =0; i<uriList.length;i++){
					Path paths = new Path(uriList[i].getPath());

					FSDataInputStream in = null;
					BufferedReader fp  = null;
					FileSystem fs2 = FileSystem.get(context.getConfiguration());
					in = fs2.open(paths);
					if (in !=null){

						fp  = new BufferedReader(new InputStreamReader(in));

						String SVline = fp.readLine();


						while (SVline != null){
							//SVline = fp.readLine();

							StringTokenizer st = new StringTokenizer(SVline," \t\n\r\f:");
							if (!st.hasMoreTokens()) break;
							vy.addElement(atof(st.nextToken()));

							//Have to do /2 because with tokenizer I take, everytime, also the feature index
							int m = st.countTokens()/2;
							//maplog.info("#### Number of tokens: " + m + " total  " + st.countTokens() );

							svm_node[] x = new svm_node[m];
							for(int j=0;j<m;j++)
							{
								x[j] = new svm_node();
								try{
									x[j].index = Integer.parseInt(st.nextToken());
									x[j].value = atof(st.nextToken());
								}catch(Exception e) {

									maplog.info("Format error from Global SV" + e);
									break;
								}
							}
							vx.addElement(x);
							maplog.info("SV record from Global SV: " + SVline);

							SVline = fp.readLine();
						}//End Loop

						maplog.info("Number of records from Global SV: " + Integer.toString(vy.size()));

						fp.close();
					}

				}
			}
		}



		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {



			InetAddress IP = InetAddress.getLocalHost();
			String IPAddress = IP.getHostAddress() ;




			String line = value.toString();
			// maplog.info(line);



			//Dividing in tokens I can take separately labels and attributes
			StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");
			//Adding to the vector the previous token
			//maplog.info(st.nextToken());
			vy.addElement(atof(st.nextToken()));

			//Have to do /2 because with tokenizer I take, everytime, also the feature index
			int m = st.countTokens()/2;

			svm_node[] x = new svm_node[m];
			for(int j=0;j<m;j++)
			{
				x[j] = new svm_node();
				x[j].index = Integer.parseInt(st.nextToken());
				x[j].value = atof(st.nextToken());
			}
			vx.addElement(x);
			//End loop/


		}



		@Override
		public void cleanup(Context context) throws IOException, InterruptedException
		{
			// do whatever you wish to do here
			maplog.info("Done Map Job!!!");

			//Setting the IP address, it will be the key
			InetAddress IP = InetAddress.getLocalHost();
			String IPAddress = IP.getHostAddress() ;

			maplog.info("Total number of records for Training: " + Integer.toString(vy.size()));
			prob.l = vy.size();
			prob.x = new svm_node[prob.l][];
			for(int i=0;i<prob.l;i++)
				prob.x[i] = vx.elementAt(i);
			prob.y = new double[prob.l];
			for(int i=0;i<prob.l;i++)
				prob.y[i] = vy.elementAt(i);




			//Specifying parameters
			svm_parameter param = new svm_parameter();
			param.probability = 1;
			param.gamma = 0.5;
			param.nu = 0.5;
			param.C = 1;
			param.svm_type = svm_parameter.C_SVC;
			param.kernel_type = svm_parameter.LINEAR;	//we decided to use Linear kernel       
			param.cache_size = 20000;
			param.eps = 0.001;      //LibSVM documentation say to use always this value of epsilon, only if we use nu-SVC we have to use 0.00001 as epsilon value

			svm_model model = svm.svm_train(prob, param);




			//Constructing SVs

			double[][] sv_coef = model.sv_coef;
			svm_node[][] SV = model.SV;
			int nr_class= svm.svm_get_nr_class(model);
			int numSV= svm.svm_get_nr_sv(model);
			String SVs;
			StringBuffer sb = new StringBuffer(); 

			for(int i=0;i<numSV;i++)
			{
				for(int j=0;j<nr_class-1;j++)

				{	
					if (sv_coef[j][i]<0.5){
						sb.append("-1");
					}
					else{
						sb.append("+1");
					}
					sb.append(" ");
				}			

				svm_node[] p = SV[i];

				sb.append(p[0].index);
				sb.append(":");
				sb.append(p[0].value);
				for(int j=1;j<p.length;j++)

				{
					sb.append(" ");
					sb.append(p[j].index);
					sb.append(":");
					sb.append(p[j].value);
					//sb.append(" ");

				}                                       

				sb.append("\n");

			}

			SVs = sb.toString();
			maplog.info("Number of SVs: " + Integer.toString(numSV));
			maplog.info("########Support vectors are: " + SVs);




			context.write(new Text(IPAddress), new Text(SVs));


			super.cleanup(context);


		}

	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {

		Logger reducelog = LoggerFactory.getLogger(Reduce.class);
		//creating the final String (will be the append of all SVs)
		private String FinalSVs = new String();

		public void reduce(Text key, Iterable<Text> values, Context context) 
				throws IOException, InterruptedException {

			//Iterator of all strings received
			Iterator<Text> it=values.iterator();
			StringBuffer sb = new StringBuffer();
			while (it.hasNext()) {

				sb.append(it.next().toString());
			}

			FinalSVs= sb.toString();
			reducelog.info("####Support vectors Reduce received: " + FinalSVs);
			context.write(new Text(""), new Text(FinalSVs));

		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path out = new Path ("globalSV");
		Logger mainlog = LoggerFactory.getLogger(Map.class);
		if (fs.exists(out)){
			long NoF = fs.getContentSummary(out).getFileCount();
			for (int i=0; i<NoF-3;i++){

				DistributedCache.addCacheFile(new URI("hdfs://master1:9000/user/lequocdo/globalSV/part-r-0000"+i),conf);
			}
		}
		Job job = new Job(conf, "SVMTraining");
		job.setJarByClass(SVMTraining.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);



		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);



	}

	private static double atof(String s)
	{
		double d = Double.valueOf(s).doubleValue();
		if (Double.isNaN(d) || Double.isInfinite(d))
		{
			System.err.print("NaN or Infinity in input\n");
			System.exit(1);
		}
		return(d);
	}

}
