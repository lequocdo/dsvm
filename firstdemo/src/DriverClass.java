package com.valerio;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class DriverClass {

	 public static void main(String[] args) throws Exception {
	        if (args.length != 2) {
	          System.out.println("usage: [input] [output]");
	          System.exit(-1);
	        }
	   
	   
	        Job job = Job.getInstance(new Configuration());
	   //specifying output type
	        job.setJobName("SVM Training");
	        job.setOutputKeyClass(Text.class);
	        job.setOutputValueClass(Text.class);
	 
//	        job.setOutputKeyClass(Object.class);
//	        job.setOutputValueClass(Object.class);
	        
	        //Specifying mapper and reducer class
	        job.setMapperClass(Mapping.class);
	        job.setReducerClass(Reducing.class); 
	 
	        job.setInputFormatClass(TextInputFormat.class);
	        job.setOutputFormatClass(TextOutputFormat.class);
	 
	        FileInputFormat.setInputPaths(job, new Path(args[0]));
	        FileOutputFormat.setOutputPath(job, new Path(args[1]));
	 
	        job.setJarByClass(DriverClass.class);
	 
	        job.submit();
	         
	         
	         
	   
	 
	   
	 }
	}