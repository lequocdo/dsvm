package org.se.mlMapReduce;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

public class SVMModelBuilding {


	public static void main(String[] args) throws Exception {

		String parameters = new String();
		String nr_class= new String();
		Integer numSV=0;
		String labels = new String();
		int num_mapper = 0;										//I need it to calculate the average  
		Vector<Double> b = new Vector<Double>();				//bias optimal hyperplane
		Vector<Double> probA = new Vector<Double>();
		Vector<Double> probB = new Vector<Double>();
		Vector<Integer> nSVs = new Vector<Integer>();
		String SVs= new String();
		String SVs_iter = new String();
		SVs="NO_IT";
		SVs_iter="ITER";
		String modelIP;											//RedisDB model IP
		int modelPort;											//RedisDB model port
		String SVsIP;											//RedisDB SVs IP
		int SVsPort;											//RedisDB SVs port


		//Reading configuration file to get IPs and ports of RedisDBs
		FileReader configfile = new FileReader(SVMModelBuilding.class.getResource("/conf/redisDBconfig.cfg").getPath());
		BufferedReader buffer  = new BufferedReader(configfile);

		//Need to use temp_port because port is an integer
		String temp_port;

		//Avoid instruction comment
		buffer.readLine();
		//Avoid comment
		buffer.readLine();
		//Getting RedisDB model IP
		modelIP = buffer.readLine();
		modelIP = modelIP.replace("IP=", "");


		//Getting RedisDB model Port
		temp_port = buffer.readLine();
		temp_port = temp_port.replace("Port=", "");
		modelPort = Integer.parseInt(temp_port);

		//Avoid comment
		buffer.readLine();
		//Getting RedisDB SVs IP
		SVsIP = buffer.readLine();
		SVsIP = SVsIP.replace("IP=", "");

		//Getting RedisDB SVs Port
		temp_port = buffer.readLine();
		temp_port = temp_port.replace("Port=", "");
		SVsPort = Integer.parseInt(temp_port);

		buffer.close();


		RedisDispatchMemory redis_model = new RedisDispatchMemory(modelIP, modelPort);

		RedisDispatchMemory redis_SVs = new RedisDispatchMemory(SVsIP, SVsPort);


		Set<String> globalmodel = redis_model.listKey("*#*");


		//Control if redisDB is empty (it will be empty at the first iteration)
		if(!globalmodel.isEmpty()){

			Iterator<String> model_iter= globalmodel.iterator();

			while(model_iter.hasNext()){
				num_mapper++;

				String temp_model = model_iter.next();
				temp_model = temp_model.replace("[", "");
				temp_model = temp_model.replace("]", "");
				StringTokenizer st = new StringTokenizer(temp_model,"#\n");

				//avoid the IP Address
				st.nextToken();

				while(st.hasMoreTokens()){
					//in this way only one mapper value is chosen (it is the same for all mappers)
					if(parameters.isEmpty()){
						parameters = "parameters"+st.nextToken().toString();
					}

					//in this way only one mapper value is chosen (it is the same for all mappers)
					if(nr_class.isEmpty()){
						nr_class= "nr_class"+st.nextToken().toString();
					}

					String temp_numSV = st.nextToken().toString();
					temp_numSV = temp_numSV.replace(";","");
					numSV += Integer.parseInt(temp_numSV);	//I'll put in DB after I have received the value from all the previous Redis entries



					String temp_b = st.nextToken().toString();
					StringTokenizer b_token = new StringTokenizer(temp_b,";");
					int k=0;


					if(b.isEmpty()){
						while(b_token.hasMoreTokens())
							b.add(Double.parseDouble(b_token.nextToken().toString()));
					}else{

						while(b_token.hasMoreTokens()){

							b.set(k, b.elementAt(k)+Double.parseDouble(b_token.nextToken().toString()));
							k++;
						}
					}


					String temp_probA = st.nextToken().toString();
					StringTokenizer probA_token = new StringTokenizer(temp_probA,";");
					int probA_k=0;


					if(probA.isEmpty()){
						while(probA_token.hasMoreTokens())
							probA.add(Double.parseDouble(probA_token.nextToken().toString()));
					}else{

						while(probA_token.hasMoreTokens()){

							probA.set(probA_k, probA.elementAt(probA_k)+Double.parseDouble(probA_token.nextToken().toString()));
							probA_k++;
						}
					}

					String temp_probB = st.nextToken().toString();
					StringTokenizer probB_token = new StringTokenizer(temp_probB,";");
					int probB_k=0;


					if(probB.isEmpty()){
						while(probB_token.hasMoreTokens())
							probB.add(Double.parseDouble(probB_token.nextToken().toString()));
					}else{

						while(probB_token.hasMoreTokens()){

							probB.set(probB_k, probB.elementAt(probB_k)+Double.parseDouble(probB_token.nextToken().toString()));
							probB_k++;
						}
					}

					//only one mapper will write the number of labels (it's the same value for all mappers)
					if(labels.isEmpty()){
						labels="labels"+st.nextToken().toString();
					}

					String temp_nSVs = st.nextToken().toString();
					StringTokenizer nSVs_token = new StringTokenizer(temp_nSVs,";");
					int nSVs_k=0;


					if(nSVs.isEmpty()){
						while(nSVs_token.hasMoreTokens())
							nSVs.add(Integer.parseInt(nSVs_token.nextToken().toString()));
					}else{

						while(nSVs_token.hasMoreTokens()){

							nSVs.set(nSVs_k, nSVs.elementAt(nSVs_k)+Integer.parseInt(nSVs_token.nextToken().toString()));
							nSVs_k++;
						}
					}

				}
			}
		}

		//get final SVs (it will be the ones used to do testing and real classification
		Set<String> globalSVs = redis_SVs.listKey("*#NO_IT*");
		//get iteration SVs, useful to do iteration using the previous SVs
		Set<String> globalSVs_iter = redis_SVs.listKey("*#ITER*");
		//Control if redisDB is empty (it will be empty at the first iteration)
		if(!globalSVs.isEmpty()){

			Iterator<String> SVs_iterator= globalSVs.iterator();

			while(SVs_iterator.hasNext()){
				StringTokenizer st = new StringTokenizer(SVs_iterator.next().toString(),"#");
				//avoid the IPAddress
				st.nextToken();
				//avoid the "NO_IT" string
				st.nextToken();
				//append the SVs
				SVs+=st.nextToken().toString();

			}
		}

		if(!globalSVs_iter.isEmpty()){

			Iterator<String> SVs_iter_iterator= globalSVs_iter.iterator();

			while(SVs_iter_iterator.hasNext()){
				StringTokenizer st = new StringTokenizer(SVs_iter_iterator.next().toString(),"#");
				//avoid the IPAddress
				st.nextToken();
				//avoid the "ITER" string
				st.nextToken();
				//append the SVs
				SVs_iter+=st.nextToken().toString();

			}
		}

		redis_model.cleardb();
		redis_SVs.cleardb();


		redis_SVs.put(SVs);
		redis_SVs.put(SVs_iter);

		for (int i=0;i<b.size();i++){
			b.set(i, b.elementAt(i)/num_mapper);
		}


		for (int i=0;i<probA.size();i++){
			probA.set(i, probA.elementAt(i)/num_mapper);
		}

		for (int i=0;i<probB.size();i++){
			probB.set(i, probB.elementAt(i)/num_mapper);
		}

		redis_model.put("numSV"+numSV.toString());


		String temp = b.toString();		//Transform vector b in a string

		//Adapt the string b to our format
		temp = temp.replace("[","");
		temp = temp.replace("]","");
		temp = temp.replace(", ", ";");
		redis_model.put("rho"+temp);


		//Doing the same done for b for probA,probB,nSVs and w
		temp = probA.toString();
		temp = temp.replace("[","");
		temp = temp.replace("]","");
		temp = temp.replace(", ", ";");
		redis_model.put("probA"+temp);


		temp = probB.toString();
		temp = temp.replace("[","");
		temp = temp.replace("]","");
		temp = temp.replace(", ", ";");
		redis_model.put("probB"+temp);


		temp = nSVs.toString();
		temp = temp.replace("[","");
		temp = temp.replace("]","");
		temp = temp.replace(", ", ";");
		redis_model.put("nSVs"+temp);


		redis_model.put(parameters);
		redis_model.put(nr_class);
		redis_model.put(labels);
	}
}

