package org.se.mlMapReduce;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SVMTraining {
	static Vector<Double> vy = new Vector<Double>();				//labels vector
	static Vector<svm_node[]> vx = new Vector<svm_node[]>();		//nodes vector
	static svm_problem prob = new svm_problem();					//problem object
	static int max_index = 0;										//need it to set gamma value
	static String modelIP;											//RedisDB model IP
	static int modelPort;											//RedisDB model port
	static String SVsIP;											//RedisDB SVs IP
	static int SVsPort;												//RedisDB SVs port

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		Logger maplog = LoggerFactory.getLogger(Map.class);
		//Setup function is used to get Support Vectors from previous interations (if they exist)
		protected void setup(Context context) throws IOException
		{


			//Reading configuration file to get IPs and ports of RedisDBs
			FileReader configfile = new FileReader(SVMTraining.class.getResource("/redisDBconfig.cfg").getPath());

			BufferedReader b  = new BufferedReader(configfile);

			//Need to use temp_port because port is an integer
			String temp_port;

			//Avoid instruction comment
			b.readLine();
			//Avoid comment
			b.readLine();
			//Getting RedisDB model IP
			modelIP = b.readLine();
			modelIP = modelIP.replace("IP=", "");


			//Getting RedisDB model Port
			temp_port = b.readLine();
			temp_port = temp_port.replace("Port=", "");
			modelPort = Integer.parseInt(temp_port);

			//Avoid comment
			b.readLine();
			//Getting RedisDB SVs IP
			SVsIP = b.readLine();
			SVsIP = SVsIP.replace("IP=", "");

			//Getting RedisDB SVs Port
			temp_port = b.readLine();
			temp_port = temp_port.replace("Port=", "");
			SVsPort = Integer.parseInt(temp_port);

			b.close();


			//Open redis connection to get Support Vectors from previous iterations
			RedisDispatchMemory redis = new RedisDispatchMemory();
			redis.init(SVsIP, SVsPort);
			Set<String> globalSV = redis.listKey("ITER*");

			//Control if redisDB is empty (it will be empty at the first iteration)
			if(!globalSV.isEmpty()){


				//Getting previous SVs and parsing them
				String prevSV = globalSV.toString();
				prevSV = prevSV.replace("[", "");
				prevSV = prevSV.replace("]", "");
				prevSV = prevSV.replace("ITER", "");
				StringTokenizer st = new StringTokenizer(prevSV,";");

				while (st.hasMoreTokens()) {

					String SVline = st.nextToken().toString();
					StringTokenizer st2 = new StringTokenizer(SVline," \t\n\r\f:");
					if (!st.hasMoreTokens()) break;

					//Getting SV label		
					vy.addElement(Double.parseDouble(st2.nextToken().toString()));



					//Have to do /2 because with tokenizer I take, everytime, also the feature index
					int m = st2.countTokens()/2;

					//Constructing nodes object
					svm_node[] x = new svm_node[m];
					for(int j=0;j<m;j++)
					{
						x[j] = new svm_node();
						try{
							x[j].index = Integer.parseInt(st2.nextToken());
							x[j].value = atof(st2.nextToken());
						}catch(Exception e) {
							break;
						}
					}

					//Adding nodes to the vector
					vx.addElement(x);
				}
			}
		}






		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {


			//Taking input vectors (Training input file)
			String line = value.toString();

			//Dividing in tokens I can take separately labels and attributes
			StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");
			//Adding to the vector the previous token
			//vy contains the labels
			vy.addElement(atof(st.nextToken()));

			//Have to do /2 because with tokenizer I take, everytime, also the feature index
			int m = st.countTokens()/2;

			//Constructing nodes object
			svm_node[] x = new svm_node[m];
			for(int j=0;j<m;j++)
			{
				x[j] = new svm_node();
				x[j].index = Integer.parseInt(st.nextToken());
				x[j].value = atof(st.nextToken());
			}
			if(m>0) max_index = Math.max(max_index, x[m-1].index);		//max_index needed to find number of feature

			vx.addElement(x);
		}



		@Override
		public void cleanup(Context context) throws IOException, InterruptedException
		{
			//Setting the IP address, it will be used to distinguish the entries from different mappers in the Redis DB
			InetAddress IP = InetAddress.getLocalHost();
			String IPAddress = IP.getHostAddress() ;

			//Setting problem variable attributes in order to use it in the training function
			prob.l = vy.size();
			prob.x = new svm_node[prob.l][];
			for(int i=0;i<prob.l;i++)
				prob.x[i] = vx.elementAt(i);
			prob.y = new double[prob.l];
			for(int i=0;i<prob.l;i++)
				prob.y[i] = vy.elementAt(i);




			//Specifying parameters
			svm_parameter param = new svm_parameter();
			param.probability = 1;							//getting also probability parameters
			param.gamma=1.0/max_index;						//gamma value	(1/num_feature)
			param.nu = 0.5;									//Useful if we want use nu-SVC or one-class SVM (default value is 0.5)
			param.C = 1;									//cost, set to default value 1
			param.svm_type = svm_parameter.C_SVC;       	
			param.kernel_type = svm_parameter.LINEAR;
			param.cache_size = 20000;						
			param.eps = 0.001;      						//LibSVM documentation say to use always this value of epsilon, only if we use nu-SVC we have to use 0.00001 as epsilon value

			//Calling Training function
			svm_model model = svm.svm_train(prob, param);

			StringBuffer sb = new StringBuffer();


			//Constructing SVs

			double[][] sv_coef = model.sv_coef;
			svm_node[][] SV = model.SV;
			int nr_class= svm.svm_get_nr_class(model);
			int numSV= svm.svm_get_nr_sv(model);
			String model_string;

			//Append nr_class
			sb.append(nr_class);
			sb.append("\n");


			//append parameters using ";" as separator
			sb.append(param.probability);
			sb.append(";");
			sb.append(param.gamma);
			sb.append(";");
			sb.append(param.nu);
			sb.append(";");
			sb.append(param.C);
			sb.append(";");
			sb.append(param.svm_type);
			sb.append(";");
			sb.append(param.kernel_type);
			sb.append(";");
			sb.append(param.cache_size);
			sb.append(";");
			sb.append(param.eps);
			sb.append("\n");


			//append nr SVs ("l" in the model object)
			sb.append(numSV);
			sb.append(";\n");

			//append rho (b values)
			for (int i=0; i< model.rho.length;i++){
				sb.append(model.rho[i]);
				sb.append(";");
			}

			sb.append("\n");

			//append probA
			for(int i=0;i< model.probA.length;i++){
				sb.append(model.probA[i]);
				sb.append(";");
			}

			sb.append("\n");

			//append probB
			for(int i=0;i< model.probB.length;i++){
				sb.append(model.probB[i]);
				sb.append(";");
			}

			sb.append("\n");


			//append labels
			for (int i=0; i< model.label.length;i++){
				sb.append(model.label[i]);
				sb.append(";");
			}

			sb.append("\n");

			//append nSVs 
			for (int i=0;i< model.nSV.length;i++){
				sb.append(model.nSV[i]);
				sb.append(";");
			}

			sb.append("\n");


			//append SVs using a new StringBuffer
			StringBuffer sb_sv = new StringBuffer();
			StringBuffer sb_iter_sv = new StringBuffer();


			for(int i=0;i<numSV;i++)
			{
				int coef_count = 0;
				for(int j=0;j<nr_class-1;j++)

				{	
					sb_sv.append(sv_coef[j][i]);					
					sb_sv.append(" ");
					if (sv_coef[j][i]<0)
						coef_count++;
				}			


				sb_iter_sv.append(model.label[coef_count]);
				sb_iter_sv.append(" ");

				svm_node[] p = SV[i];


				sb_sv.append(p[0].index);
				sb_sv.append(":");
				sb_sv.append(p[0].value);
				//useful for iterations
				sb_iter_sv.append(p[0].index);
				sb_iter_sv.append(":");
				sb_iter_sv.append(p[0].value);
				for(int j=1;j<p.length;j++)

				{
					sb_sv.append(" ");
					sb_sv.append(p[j].index);
					sb_sv.append(":");
					sb_sv.append(p[j].value);

					//useful for iterations
					sb_iter_sv.append(" ");
					sb_iter_sv.append(p[j].index);
					sb_iter_sv.append(":");
					sb_iter_sv.append(p[j].value);
				}                                       


				sb_sv.append(";");
				sb_iter_sv.append(";");
			}



			model_string = sb.toString();
			String SVs = sb_sv.toString();
			String iter_SVs = sb_iter_sv.toString();
			model_string += SVs;
			model_string += "@";			//Divide final SVs from SVs for iterations with "@" character
			model_string += iter_SVs;


			//Sending to Reducer
			context.write(new Text(IPAddress), new Text(model_string));

			super.cleanup(context);
		}
	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {


		public void reduce(Text key, Iterable<Text> values, Context context) 
				throws IOException, InterruptedException {

			int num_mapper = 0;	//I need it to calculate the average  
			Vector<Double> b = new Vector<Double>();			//bias optimal hyperplane
			Vector<Double> probA = new Vector<Double>();
			Vector<Double> probB = new Vector<Double>();
			Integer numSV =0;
			Vector<Integer> nSVs = new Vector<Integer>();


			//Reading configuration file to get IPs and ports
			FileReader configfile = new FileReader(SVMTraining.class.getResource("/redisDBconfig.cfg").getPath());

			BufferedReader buffer  = new BufferedReader(configfile);

			//Need to use temp_port because port is an integer
			String temp_port;

			//Avoid instruction comment
			buffer.readLine();
			//Avoid comment
			buffer.readLine();
			//Getting RedisDB model IP
			modelIP = buffer.readLine();
			modelIP = modelIP.replace("IP=", "");


			//Getting RedisDB model Port
			temp_port = buffer.readLine();
			temp_port = temp_port.replace("Port=", "");
			modelPort = Integer.parseInt(temp_port);

			//Avoid comment
			buffer.readLine();
			//Getting RedisDB SVs IP
			SVsIP = buffer.readLine();
			SVsIP = SVsIP.replace("IP=", "");

			//Getting RedisDB SVs Port
			temp_port = buffer.readLine();
			temp_port = temp_port.replace("Port=", "");
			SVsPort = Integer.parseInt(temp_port);

			buffer.close();


			RedisDispatchMemory redis_model = new RedisDispatchMemory();
			redis_model.init(modelIP, modelPort);			//DB for model parameters
			RedisDispatchMemory redis_SVs = new RedisDispatchMemory();
			redis_SVs.init(SVsIP, SVsPort);			//DB for SVs


			//clear redis DBs from previous SVs and previous model
			redis_SVs.cleardb();
			redis_model.cleardb();
			//Iterator of all strings received
			Iterator<Text> it=values.iterator();


			while (it.hasNext()) {

				num_mapper++;
				StringTokenizer st = new StringTokenizer(it.next().toString(),"\n");

				//Check if nr of class was already inserted,if not we insert it
				Set<String> temp_nrclass = redis_model.listKey("nr_class*");
				if (temp_nrclass.isEmpty()){
					redis_model.put("nr_class"+st.nextToken().toString());
				}

				//Check if problem parameters were already inserted,if not we insert them
				Set<String> temp_parameters = redis_model.listKey("parameters*");
				if (temp_parameters.isEmpty()){
					redis_model.put("parameters"+st.nextToken().toString());
				}


				//I'll put numSV value in DB after I have received the value from all mappers
				String temp_numSV = st.nextToken().toString();
				temp_numSV = temp_numSV.replace(";","");
				numSV += Integer.parseInt(temp_numSV);		//sum with values already received	


				//I'll put b in the database after all mappers have sent its value
				String temp_b = st.nextToken().toString();
				StringTokenizer b_token = new StringTokenizer(temp_b,";");
				int k=0;

				if(b.isEmpty()){
					while(b_token.hasMoreTokens())
						b.add(Double.parseDouble(b_token.nextToken().toString()));
				}else{

					while(b_token.hasMoreTokens()){

						b.set(k, b.elementAt(k)+Double.parseDouble(b_token.nextToken().toString()));
						k++;
					}
				}


				String temp_probA = st.nextToken().toString();
				StringTokenizer probA_token = new StringTokenizer(temp_probA,";");
				int probA_k=0;

				if(probA.isEmpty()){
					while(probA_token.hasMoreTokens())
						probA.add(Double.parseDouble(probA_token.nextToken().toString()));
				}else{

					while(probA_token.hasMoreTokens()){

						probA.set(probA_k, probA.elementAt(probA_k)+Double.parseDouble(probA_token.nextToken().toString()));
						probA_k++;
					}
				}


				String temp_probB = st.nextToken().toString();
				StringTokenizer probB_token = new StringTokenizer(temp_probB,";");
				int probB_k=0;

				if(probB.isEmpty()){
					while(probB_token.hasMoreTokens())
						probB.add(Double.parseDouble(probB_token.nextToken().toString()));
				}else{

					while(probB_token.hasMoreTokens()){

						probB.set(probB_k, probB.elementAt(probB_k)+Double.parseDouble(probB_token.nextToken().toString()));
						probB_k++;
					}
				}



				redis_model.put("labels"+st.nextToken().toString());

				String temp_nSVs = st.nextToken().toString();
				StringTokenizer nSVs_token = new StringTokenizer(temp_nSVs,";");
				int nSVs_k=0;


				if(nSVs.isEmpty()){
					while(nSVs_token.hasMoreTokens())
						nSVs.add(Integer.parseInt(nSVs_token.nextToken().toString()));
				}else{

					while(nSVs_token.hasMoreTokens()){

						nSVs.set(nSVs_k, nSVs.elementAt(nSVs_k)+Integer.parseInt(nSVs_token.nextToken().toString()));
						nSVs_k++;
					}
				}



				String SVs = st.nextToken().toString();
				//Divide SVs for iteration from final SVs
				StringTokenizer SVs_token = new StringTokenizer(SVs,"@");

				//Control if this is the first mapper which sent me data
				Set<String> temp = redis_SVs.listKey("NO_IT*");
				if (temp.isEmpty()){
					redis_SVs.put("NO_IT"+SVs_token.nextToken().toString());
				}else{
					String temp1 = temp.toString();
					temp1 = temp1.replace("[", "");
					temp1 = temp1.replace("]", "");
					redis_SVs.put(temp1+SVs_token.nextToken().toString());		//append the the SVs received from other mappers
				}

				temp = redis_SVs.listKey("ITER*");

				if (temp.isEmpty()){
					redis_SVs.put("ITER"+SVs_token.nextToken().toString());
				}else{
					String temp1 = temp.toString();
					temp1 = temp1.replace("[", "");
					temp1 = temp1.replace("]", "");
					redis_SVs.put(temp1+SVs_token.nextToken().toString());		//append the the SVs received from other mappers
				}		
			}

			//calculating the b averages
			for (int i=0;i<b.size();i++){
				b.set(i, b.elementAt(i)/num_mapper);
			}


			for (int i=0;i<probA.size();i++){
				probA.set(i, probA.elementAt(i)/num_mapper);
			}

			for (int i=0;i<probB.size();i++){
				probB.set(i, probB.elementAt(i)/num_mapper);
			}

			redis_model.put("numSV"+numSV.toString());


			String temp = b.toString();		//Transform vector b in a string

			//Adapt the string b to our format
			temp = temp.replace("[","");
			temp = temp.replace("]","");
			temp = temp.replace(", ", ";");
			redis_model.put("rho"+temp);


			//Doing the same done for b for probA,probB,nSVs and w
			temp = probA.toString();
			temp = temp.replace("[","");
			temp = temp.replace("]","");
			temp = temp.replace(", ", ";");
			redis_model.put("probA"+temp);


			temp = probB.toString();
			temp = temp.replace("[","");
			temp = temp.replace("]","");
			temp = temp.replace(", ", ";");
			redis_model.put("probB"+temp);


			temp = nSVs.toString();
			temp = temp.replace("[","");
			temp = temp.replace("]","");
			temp = temp.replace(", ", ";");
			redis_model.put("nSVs"+temp);
		}
	}


	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "SVMTraining with MapReduce");
		job.setJarByClass(SVMTraining.class);
		job.setNumReduceTasks(1);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);



		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);



	}


	//Function to control input is in correct format
	private static double atof(String s)
	{
		double d = Double.valueOf(s).doubleValue();
		if (Double.isNaN(d) || Double.isInfinite(d))
		{
			System.err.print("NaN or Infinity in input\n");
			System.exit(1);
		}
		return(d);
	}

}
