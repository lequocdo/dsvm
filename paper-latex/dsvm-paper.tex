\documentclass[10pt, conference]{IEEEtran} 
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage{amstext}
\usepackage{booktabs}
\usepackage{amsmath} 
\usepackage{tabu}

%\usepackage[•]{•}1]{fontenc}
%\usepackage{subfig}
\usepackage{caption}
\usepackage{subfigure}
\usepackage{lipsum}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{color}
\usepackage{xcolor}
\usepackage{listings}
\usepackage[]{algorithm2e}

\DeclareMathOperator{\argmax}{argmax\text{  }}
\DeclareMathOperator{\argmin}{argmin\text{  }}

\lstset{ 
    language=C, % choose the language of the code
    basicstyle=\fontfamily{pcr}\selectfont\footnotesize\color{gray},
    keywordstyle=\color{black}\bfseries, % style for keywords
    %numbers=none, % where to put the line-numbers
    numberstyle=\tiny, % the size of the fonts that are used for the line-numbers     
    backgroundcolor=\color{white},
    showspaces=false, % show spaces adding particular underscores
    showstringspaces=false, % underline spaces within strings
    showtabs=false, % show tabs within strings adding particular underscores
    %frame=B, % adds a frame around the code
    tabsize=2, % sets default tabsize to 2 spaces
    rulesepcolor=\color{gray},
    rulecolor=\color{black},
    captionpos=b, % sets the caption-position to bottom
    breaklines=false, % sets automatic line breaking
    breakatwhitespace=false, 
    %language=C,
    basicstyle=\small\sffamily,
    numbers=left,
    xleftmargin=2em,frame=single,framexleftmargin=1.5em,
    frame=tb,
    columns=fullflexible,
}



\title{Scalable Network Traffic Classification Using Distributed Support Vector Machines}


\author{\IEEEauthorblockN{Do Le Quoc\IEEEauthorrefmark{1},
Valerio D'Alessandro\IEEEauthorrefmark{2}, 
Byungchul Park\IEEEauthorrefmark{4},
Luigi Romano\IEEEauthorrefmark{3}, 
Christof Fetzer\IEEEauthorrefmark{1}} 
\IEEEauthorblockA{
\IEEEauthorrefmark{1}Systems Engineering Group \\
					Dresden University of Technology\\
					Dresden, Germany\\
					Email: \{do.le\_quoc, christof.fetzer)\}@tu-dresden.de\\
\IEEEauthorrefmark{2}University of Naples Federico II\\
                    Naples, Italy\\
                    Email: vale.dalessandro@studenti.unina.it\\
\IEEEauthorrefmark{3}Parthenope University of Naples\\
                    Naples, Italy\\
                    Email: luigi.romano@uniparthenope.it\\
\IEEEauthorrefmark{4}University of Toronto\\
                    Toronto, Canada\\
                    Email: byungchul.park@utoronto.ca                     
        }
}

\begin{document}
\maketitle

\begin{abstract}

Internet traffic has increased dramatically in recent years due to the popularization of the Internet and the appearance of wireless Internet mobile devices such as smart-phones and tablets. The explosive growth of Internet traffic has introduced a practical example that demonstrates the concept of Big Data. Accurate identification and classification of large network traffic data play an important role in network management including capacity planning, network forensics, QoS and intrusion detection. However, the state-of-the-art solutions, which rely on a dedicated server, are not scalable for analyzing high volume network traffic data. In this paper, we implement a distributed Support Vector Machines (SVMs) framework for classifying network traffic using Hadoop, an open-source distributed computing framework for Big Data processing. We design a global parameter store that maintains the global shared parameters between SVM training nodes. The distributed SVMs has been deployed on a 20 node cluster to analyze real network traffic trace. The results demonstrate that with 19 Mapper nodes the system is around 30\% faster than CloudSVM \cite{cloudsvm} solution and outperforms the standalone SVM with nearly 9 times faster in training process and 15 times in the classifying process. In addition, the distributed SVMs architecture is designed to analyze large scale datasets. Therefore, it can be used not only for processing network traffic dataset, but also other large scale datasets such as WWW data.
\end{abstract}

\section{Introduction}

Internet traffic analytics has been the subject of intensive study since the birth of the Internet itself. The concept of network traffic analytics includes network traffic monitoring and network traffic classification. The objective of traffic classification is to automatically recognize applications that have generated traffic from the active and passive capturing of individual packets, or streams of packets in networks. The information provided by network traffic analytics is extremely valuable in network management and network security areas. Indeed, accurate traffic classification assists efficient management of existing network resources by providing more accurate resource allocation mechanisms in cloud computing environments. It also supports several network management related tasks, such as Quality-of-Service (QoS), trend analysis, pricing and network security \cite{serveypayload}. In addition, network traffic classification plays an important role in customer behavior analysis, which has been attracting and received increasing attention in both academic and industry area in the recent years. Service providers could use an efficient traffic discrimination system deployed in their network to comprehend the behavior of their customers and from that they could provide more personalized services to achieve greater customer satisfaction.

Machine learning (ML) techniques are widely used in data mining in general and network traffic analysis in particular. Support vector machine (SVM) is one of the most popular supervised learning algorithms for both regression and classification problems due to robustness and high accuracy. The SVM algorithm is formulated as a convex quadratic programming optimization problem with linear constraints, which can be solved using the Interior-Point method \cite{mehrotra1992}. However, over the last five years, the growth of the Internet traffic volume demonstrates the notion of Big Data. This obstacles to apply SVMs in data mining because the SVMs runtime could scale approximately cubically with the number of observations in the large training dataset \cite{tsang2005}. Moreover, the large datasets do not fit into memory and even in the hard disk of a single machine.  To overcome the  problems of Big Data, the parallel and distributed methods for SVM training have been intensively studied. To reduce the time spent in kernel SVM training on large-scale datasets, Graf \textit{et al.} \cite{cascadesvm} introduced Cascade SVM which is a multilevel approach. The original training dataset is partitioned into subsets. Then, an SVM is used to train each subset using the same parameters as the global SVM problem. The support vectors (SVs) obtained by these SVMs are combined to pass as inputs to the next training layer. This process is continued through all of the levels. A single list of SVs is collected at the final level. This list then is included to each of the input subsets at the first level, and the training process on the tree topology is repeated for the next iteration. The Cascade SVM training is continued until the same list of SVs is produced in two consecutive iterations. Cascade SVM guarantees converging to the global solution. However, the training processes in iterations may be duplicated in practice because the same SVs are added in each training iteration. Furthermore, Cascade SVM is poorly scalable and inflexible since it needs to predefine the network topology. How to partition data in an elegant way is another issue in Cascade SVM. Based on the idea of Cascade SVM, several works were proposed using Hadoop\footnote{http://hadoop.apache.org/} framework to implement MapReduce based SVM \cite{cloudsvm} \cite{ensemblesvm}. Instead of using multiple training layers like Cascade SVM, only one training layer is deployed on Mapper nodes. In their work, they did not mention how to split training dataset to maintain load balancing between Mapper nodes during training processes. Catak \cite{cloudsvm} implemented CloudSVM using Hadoop streaming (mrjob\footnote{https://github.com/Yelp/mrjob}) that would slow down the training process, whereas Alham \textit{et al.} \cite{ensemblesvm} evaluated their work with only binary classification datasets. 

In addition, MapReduce is not suitable for SVM training since SVM training is global optimization process. The training dataset is partitioned to small datasets, each Mapper can only obtain support vectors locally based on the small dataset in one partition.

In this work, to achieve the global optimization, we supplement a global parameter store based on the parameter server concept \cite{parameterserver}\cite{mlbase}  that maintain the global shared parameters during the training process. The training process will be performed on the subset of the training data in each Mapper node and the intermediate parameters will be pushed/pulled to/from the parameter store in each iteration of the training process. To speed up the training process and maintain the consistency of the shared parameters, we use Redis\footnote{http://redis.io/}, an in-memory key-value database, to implement the global parameter store. Our design can be used to process not only network traffic datasets, but also other large scale datasets. For example, the proposed framework can be applied to analyze the Web data to extract business intelligence.

In this paper, our contributions are as follows. First, we survey of state-of-the-art approaches that use Big Data solutions to analyze big network traffic data (Section II). Second, we propose distributed SVMs architecture for classifying network traffic using Hadoop and the global parameter store (Section III). Next, we present in detail the implementation of the proposed architecture (Section IV). Finally, our experimental evaluation on real network traffic datasets in a distributed environment illustrates that the proposed approach can achieve the scalability in processing big network traffic data and speed up 30 \% training time compared to CloudSVM \cite{cloudsvm} (Section IV).  

\section{Related Work}
The network traffic analyzing approaches can be classified in four main groups namely port-based, payload-based, statistical and machine learning-based, and Big Data based approach. Port-based approaches use the port number in the packet header to identify the application traffic. The disadvantage of this approach is unreliable since emergence of the many P2P applications that pick randomly port numbers. Meanwhile, payload-based approaches try to look at packet's payloads from network traffic to identify a set of patterns or signatures to classify network traffic. This approach cannot be applied to classify encrypted traffic. Unlike the payload-based approach, the statical and machine learning based approach measure the unique statistical characteristic of network traffic generated by different applications to classify encrypted traffic. In this section, we focus only on the state-of-the-art of Big Data based approaches for analyzing network traffic.

With the rapid growth of the Internet traffic volume, the traditional stand-alone solutions for network traffic analytics have faced enormous challenges related to computing efficiency and storage capacity. The traditional solutions are usually conducted in a single high-performance server that is not able to analyze data at Terabyte (TB) and Petabyte (PB) levels in a reasonable time. Meanwhile, Google has shown the ability to cope with Big Data issues using MapReduce and Google File System (GFS) \cite{mr2008}. As a result, there is recent interest in using MapReduce for analyzing network traffic \cite{yen2013} \cite{quang2013}. MapReduce \cite{mr2008} is a programming model and a framework for processing large data sets, providing fault tolerance and high scalability to Big Data processing. Most of these works have used Hadoop, an open-source distributed computing framework implementing the MapReduce programming model and the Hadoop Distributed File System (HDFS) as its distributed file system similar to GFS.

RIPE \cite{ripe} proposed a packet traffic analysis library for Hadoop, but it does not consider the parallel processing capabilities of analyzing packet records from the HDFS blocks of a file, which leads to a lack of scalability. Lee \textit{et al.} \cite{yen2013} presented a Hadoop-based packet trace processing tool to process large amounts of binary network traffic. A new input type to Hadoop was developed, the PcapInputFormat, which encapsulates the complexity of processing a captured pcap trace to extract the packets. However, the PcapInputFormat uses a timestamp (using sliding-window) to find the first record from each block, which would reduce accuracy of the analysis. Moreover, they presented applying their proposed framework only on collecting periodic statistic network traffic data such as total traffic size and  number of hosts, packets, not classifying network traffic data. 

Aryan \textit{et al.} \cite{store2013} presented a mechanism for scalable storage and real-time processing of monitoring data. In their work, they used HBase\footnote{http://hbase.apache.org/}, which is inspired from Google's BigTable database \cite{bigtable} to store network flow data. HBase is a distributed, fault-tolerant, scalable database that runs on top of the HDFS file system and provides random real-time read/write access to data. A table (reference table) is used to store all traffic flows and other tables for indexing to provide a quick response time for analyzing queries. However, the supported queries are very restrictive. Most of them are finding top-N elements queries.

In general, Hadoop has been widely used for various large scale tasks. However, the framework does not provide low latency. Indeed, Hadoop was initially designed for batch processing. It uses a staged approach where one stage cannot start before the preceding stage has finished. This approach not only adds considerable latency, but may also limit scalability \cite{do2013}. Therefore, a quick analysis of big network traffic data still remains a challenge.

In a previous work \cite{do2013}, we mitigate the above problem by introducing a scalable DPI system with MapReduce programming model using StreamMine3G framework\footnote{https://streammine3g.inf.tu-dresden.de/trac} that is able to analyze high throughput network traffic data in real-time. We distribute the inspection engine (L7-Filter) of the DPI system across many machines to analyze network traffic, so that network traffic is processed in parallel on multiple machines in order to reduce the computational overhead and memory consumption which are distributed across many machines. However, as mentioned above, the DPI system cannot process encrypted network traffic. This motivates us to investigate a distributed SVM system to analyze network traffic.


\section {Distributed Support Vector Machines}

 SVM is a supervised learning method for classification and regression analysis.
The SVM has been extensively used for network traffic analysis \cite{svmtcp} \cite{kim2008}. In this section, we introduce a MapReduce based distributed SVMs mechanism that runs in parallel on multiple nodes of a cluster. 
 
\subsection{Architecture} 
 In our design, we supplement MapReduce architecture for iteration algorithms (SVM training process) by introducing a global parameter store to maintain shared parameters between (Fig. \ref{fig:dsvm}) training nodes in each iteration to achieve the global optimization.

In particular, the training set is divided into n smaller subsets and loaded to n corresponding Mapper nodes of a cluster. In Map phase, the SMO algorithm is used for training each of subsets located in Mapper nodes. The training on the subsets yield partial SVs and local model parameters ($\mathbf{w}$ and $b$), after that in the Reduce phase the entire set of the SVs and the model parameters are combined and push to the global parameter store.

\begin{figure}[htp]
\centering
\includegraphics[width=0.48\textwidth]{figures/DSVM.pdf}
\caption{MapReduce SVM architecture.}
\label{fig:dsvm}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{figures/MapSVM.pdf}
\caption{MapNoReduce SVM architecture.}
\label{fig:mapsvm}
\end{figure}

Nevertheless, the initial global list is not equivalent to the optimal solution of the whole training dataset. Therefore, a feedback loop mechanism (Fig. \ref{fig:dsvm}) is applied to ensure the Distributed SVM converges to the global optimal solution. The original training dataset is partitioned into subsets again, and each Mapper node pull the global SVs from the parameter store to add into the local training subsets. A new set of SVs and model parameters is obtained after the Map phase as in the first pass, and the process continues until the list of SVs or the test accuracy value does not change, that means the training process provably converges to the global optimum. The proof of the convergence was presented in \cite{cloudsvm}. The algorithm for distributed SVM training process is presented in Algorithm \ref{algorithm1}.

\begin{algorithm}%[H]

 \KwIn{the training data $D; D=\{x_i, y_i\},\ i={(1..n)}$}
 \KwOut{Model(SV, W, b)}
 \textbf{\underline{Initialization:}}\\
 Split the training dataset to smaller datasets $D_i$  \\
 Load the smaller training data $D_i$ to all Mapper nodes\\
 $i={(1..n)}$ // n is the number of Mapper nodes\\
 $t\leftarrow 0$; \\
 \SetAlgoLined
 \SetKwProg{Fn}{Function}{}{end}
 \textbf{\underline{Mapper nodes:}}\\
 \Fn{Training(t)}{
 \While{$h^{(t)} \neq  h^{(t-1)}$}{
  pull $SV^{(t-1)}$ from the global parameter store\\
  $D_i \leftarrow D_i \cup  SV^{(t-1)}$
  //Using SMO algorithm for the SVM training \\
  $SV^{(t)}, W^{(t)}, b^{(t)}, h^{(t)} \leftarrow SVM-training(D_i)$ \\
  push $SV^{(t)}, W^{(t)}, b^{(t)}$ to the global parameter store\\
 }
 }
 
 \textbf{\underline{Master node:}}\\
 \Fn{createModel(t)}{
 Pull $W^{(t)}, b^{(t)}$ from the global parameter store\\
 $W\leftarrow \sum^{n}_{i=0}{W^{(t)}}$ \\
 $b\leftarrow (\sum^{n}_{i=0}{b^{(t)}})/n$\\
}


\caption{Distributed SVM Training.}
\label{algorithm1} 
\end{algorithm}

The global parameter store contains the shared parameters between Mapper nodes. Therefore, it needs to ensure a consistent view of the share parameters for all Mapper nodes.

Unlike previous approaches, the training process in our design is performed in Map phase only. Therefore, to gain performance benefit, the combination intermediate parameters and creating model process are moved to Master node side (Fig. \ref{fig:mapsvm}). The creating model process is needed for testing the accuracy of the model in each iteration. Since the proposed architecture did not contain any Reduce phase, it is called MapNoReduce SVM architecture. 

While other approaches contain the transferring time of data between Mapper and Reducer nodes (Shuffle and Sort time) and between Reduce nodes to the global parameter store (usually HDFS files) as well as the computation time in Mapper and Reducer nodes, the time cost of our approach includes only two parts,  computation time in Mapper nodes and transferring time between Mapper nodes and the global parameter store. The transferring time of data between Mapper nodes and the global parameter store that depends on the latency of the network connection between them, so the global parameter store nodes should be deployed in the same rack as Mapper nodes.


\subsection{Support Vectors Message}
The support vectors shared between Mapper nodes can be presented as a set of key/value pairs which the key is a feature ID and the value is the weight belong the feature ID. The key/value message is wildly used in practice \cite{mlbase}\cite{parameterserver}. In our work, for simplicity, we used only the key to represent support vectors that including feature IDs and their weight. To reduce the communication traffic between Mapper nodes and the global parameter store, the SV messages could be compressed.

The Mapper nodes share the parameters by communicating with the global parameter store using \textit{put} and \textit{get} operations. Each Mapper node pushes its local SVs to the global parameter store and pulls the global SVs back.

\section{Implementation}

We used Hadoop, an open-source implementation of MapReduce framework, to implement the distributed SVMs since Hadoop has been widely used in both academic and industrial areas.
\subsection{Global Parameter Store}
To implement the global parameter store that ensure requirements of our design described in Section III, we use Redis\footnote{http://redis.io} to store the shared parameters. Redis is a high performance in-memory  key-value store. Using Redis to store the shared SVs and model parameters not only provides fast processing but also helps to remove duplicate SVs in the global SVs list, because Mapper nodes could push the same local SVs to the global parameter store. Moreover, Redis supports pattern queries to get a list of keys. For example, in each Mapper node, to pull a list of shared SVs from the global parameter store, the function $redis.keys(SV*)$ will be called. 

\subsection{SVM Training}
In this paper, LibSVM\cite{libsvm} is used to train each subset of the training dataset in Mapper nodes. LibSVM is known as the most efficient SVM implementation software. It is an integrated software for SVM classification, regression, and distribution estimation. It contains almost efficient analysis models such as C-SVC and nu-SVC classification models, epsilon-SVR and nu-SVR regression models, and one-class SVM distribution estimation.  Cross validation is adopted to improve the classification correct rate. The computation time complexity of libSVM is $O(n^{2})$. The detail information of libSVM can be found in \cite{libsvm}.

\subsection{SVM Classifying}
For testing or classifying, LibSVM is also used as the training phase (Section IV.B) to classify the testing dataset in Mapper nodes.


\subsection{Feature Extraction for Network Traffic Classification}

\begin{figure}[htp]
\centering
\includegraphics[width=0.42\textwidth]{figures/netsvm.pdf}
\caption{Distributed SVM based network traffic analysis.}
\label{fig:netsvm}
\end{figure}

Fig. \ref{fig:netsvm} illustrates how to apply the proposed distributed SVM to analyze network traffic data. We have implemented the feature extraction module to aggregate packets into 5-tuple flows (packets have the same source and destination IP addresses, source and destination ports, and transport protocol) and to extract features that are used for the distributed SVM classification. Each flow is represented with a set of features including source port, destination port, protocol, number of packets belong to the flow, size of the flows, size of the first packet, size of the second packet, size of the third packet, size of the fourth packet in the flow, and duration. The features are selected based on \cite{onlinesvm}, \cite{svmfeatures} that proved that packet sizes carry enough information to identify the application layer protocol behind a network flow with high accuracy and first four packet sizes are the optimal value in the trade-off between accuracy and computation cost. In the training phase, the module is used to extract features from a dataset for creating a training dataset. Each flow is labeled with application ID that represents a record in the training dataset. The dataset is loaded to a Hadoop cluster where the proposed distributed SVM is deployed on. A SVM model is built after a certain number of iterations of the training job. The model is deployed in Mapper nodes in the classifying phase. The feature extraction module is used again to extract flows with features from target network traffic source such as pcap files or network links for classifying.

\section{Evaluation}
We use a 20 nodes cluster where nodes are connected via Gigabit Ethernet (1000BaseT full duplex). Each node has 2 Intel Xeon E5405 CPUs (quad core) and 8GB of RAM. Hard disks are attached via SATA-2. The operating system in each node is Debian Linux 5.0 with kernel 2.6.26.

\subsection{Training Processing}

We used a dataset from CBA Lab\footnote{http://www.cba.upc.edu/monitoring/traffic-classification\#is-our-ground-truth-for-traffic-classification-reliable-dataset} to validate our approach. The dataset contains 35.69 GB of pure packet data with 1,262,022 flows captured during 66 days (between February 25, 2013 and May 1, 2013) \cite{vbs}. The dataset has three pcap traces with pure packet data, one for each OS used (Linux, Windows XP, and Windows 7), and three INFO files that contain flows of the pcap traces. Each line in an INFO file corresponds to a flow in the pcap traces with information as follows:

flowID \# startTime \# endTime \# srcIP \# dstIP \# srcPort \# dstPort \# transportProtocol \# operatingSystem \# processName \# HTTP Url \# HTTP Referer \# HTTP Content-type \#.

41.28 \% of all the flows (520,993 flows) were labelled with process names.

The author \cite{vbs} of the dataset developed and adapted Volunteer-Based System (VBS)\footnote{http://vbsi.sourceforge.net/} to collect and accurately label the flows. From the system sockets, VBS was used to identify the process name associated with each flow. By this way, VBS can ensure that the application is associated to a particular traffic.

We used the feature extraction module (Section IV.C) to extract flows from the three pcap traces and label the flows with Application IDs (40 applications or process names in the dataset)\footnote{https://bitbucket.org/lequocdo/DSVM/applicationlist.txt} using the three INFO files to build training dataset. 



The training dataset was loaded to the Hadoop cluster using \textit{Put} command provided by Hadoop. By default, Hadoop splits input files based on the total size (in bytes) of the input files, and distributes the files over all nodes in HDFS. Therefore, when the training dataset is pushed to the cluster, Hadoop will split the dataset into imbalanced small subsets where there is only one class or a few classes of the training dataset. The Mapper nodes that store one class in local training subsets, cannot perform the training task until receiving global SVs from the global parameter store in the next iteration of training process. This would waste the computing resource and increase the training time. Moreover, the imbalanced data sets could have only few records of majority classes in the training dataset that would affect the training performance. To avoid these issues, we developed a preprocessing job that splits the training data based on the number of records of each class using a \textit{Hash} function, instead of using the total size of the training dataset files. The preprocessing program allows our system to evenly distribute the training computation overhead to all nodes. Fig. \ref{fig:cpuram} respectively illustrates the load balancing in terms of memory and CPU utilization of six slave nodes in an iteration of the training process. Meanwhile, Fig. \ref{fig:cpuramrandom} presents the imbalanced load, including memory and CPU consumption, between six slave nodes in an iteration when the preprocessing was not performed in the training process.


\begin{figure*}
 \centering
  \begin{tabular}{cc}
  \centering
    \begin{minipage}[t]{\columnwidth}
    \centering
      \includegraphics[width=0.9\textwidth]{figures/svmram.pdf}
      % \caption{.}
      % \label{fig:synctime-vs-size-hdd}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
    \centering
      \includegraphics[width=0.9\textwidth]{figures/svmramrandom.pdf}
      % \caption{.}
      % \label{fig:synctime-vs-size-ssd}
    \end{minipage}
    \\
    \begin{minipage}[t]{\columnwidth}
    \centering
      \includegraphics[width=0.9\textwidth]{figures/svmcpu.pdf}
      \caption{Memory and CPU utilization in SVM training process with preprocessing the training dataset.}
      \label{fig:cpuram}
    \end{minipage}
    &
    \begin{minipage}[t]{\columnwidth}
    \centering
      \includegraphics[width=0.9\textwidth]{figures/svmcpurandom.pdf}
      \caption{Memory and CPU utilization in SVM training process without preprocessing the training dataset.}
      \label{fig:cpuramrandom}
    \end{minipage}
  \end{tabular}
\end{figure*}



%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.42\textwidth]{figures/svmram.pdf}
%\caption{Memory utilization in SVM training process with preprocessing the training dataset.}
%\label{fig:svmram}
%\end{figure}
%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.42\textwidth]{figures/svmcpu.pdf}
%\caption{CPU utilization in SVM training process with preprocessing the training dataset.}
%\label{fig:cpusvm}
%\end{figure}
%
%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.42\textwidth]{figures/svmramrandom.pdf}
%\caption{Memory utilization in SVM training process without preprocessing the training dataset.}
%\label{fig:svmramrandom}
%\end{figure}
%
%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.42\textwidth]{figures/svmcpurandom.pdf}
%\caption{CPU utilization in SVM Training process without preprocessing the training dataset.}
%\label{fig:svmcpurandom}
%\end{figure}

To measure the accuracy of the built models, we removed unnecessary application records such as \textit{svchost}, \textit{System}, and \textit{java} application in the training dataset and used 10-fold cross validation. This means that we divided the training dataset into 10 roundly equal-size parts. Then, we used 90\% of the training dataset (9 parts) for training to create a model and used the model to test the remain part. This process was repeated until all parts were used for the testing process. Table I reports the accuracy of the models after various training iterations with 19 Mapper nodes using 10-fold cross validation on the training dataset.

\begin{table}[h]
\center
\begin{tabular}{c|c|c}
%\cline{2-3}
\multicolumn{1}{l|}{} & \textbf{\#Iteration} & \textbf{Accuracy}    \\ \hline
\multicolumn{1}{c|}{\textbf{Single super node}}    & 1           & 92.8305\% \\ \hline
\multicolumn{1}{c|}{\textbf{19 Mapper nodes}} & 1           & 91.4885\% \\ \hline
\multicolumn{1}{c|}{\textbf{19 Mapper nodes}} & 2           & 92.9178\% \\ \hline
\multicolumn{1}{c|}{\textbf{19 Mapper nodes}} & 3           & 93.1333\% \\ 
\end{tabular}
\caption{Accuracy of the models after training iterations}
\end{table}

 We implemented the proposed distributed SVM using the Java programming language and Redis for storing SVs and coefficients, unlike \cite{cloudsvm}, that used Python Hadoop streaming to implement CloudSVM that significantly slows down the training processing. In order to show the effectiveness of the proposed system, we also implemented CloudSVM (\cite{cloudsvm}) using Hadoop streaming and used CloudSVM as a baseline.  Fig. \ref{fig:svmtraining} illustrates how the addition of nodes reduces the training time consumption and how the scalability of the processing capacity is achieved in training the network traffic dataset. MapNoReduce and MapReduce SVM using Redis are around 30\% faster than CloudSVM and outperform the standalone SVM with nearly 9 times faster with 19 nodes in terms of training time. To perform the training processing on a standalone machine, we use one node in CloudandHeat\footnote{https://www.cloudandheat.com/} system with 8 VCPUs and 32 GB of RAM.  Compared to MapReduce with Redis, MapNoReduce does not improve performance much because the most time consuming part is the training in Map phase and the dataset is not really big. However, we believe that MapNoReduce will gain much more performance benefit in processing larger datasets. 

\subsection{Classifying Processing}
After the training phase, a SVM model is created and the SVM model is deployed on each Mapper node for classifying network traffic datasets (Fig. \ref{fig:netsvm}) using Hadoop without any modifications. To validate the performance of the classifying process using the proposed distributed SVM approach, we use a dataset with a 101 GB pcap file that contains network traffic captured from the Internet conjunction of a network company in Korea during 32 minutes. The trace file consists of 88,259,010 packets with 389,056 flows. We also use the feature extraction module to extract flows from the trace to build the testing dataset. We used the NAVL engine\footnote{http://www.proceranetworks.com/deep-packet-inspection} developed by Vineyard Networks to label flows with applications name. Table III shows the applications traffic profile of the trace. Since the application lists in the training dataset is different with the application list of the testing dataset, they have only few overlap applications such as \textit{bitorrent}, \textit{http} and \textit{ssh}. Therefore, it is difficult to measure the accuracy of the classifying of all applications that used the model in the training process and the profiling information from NAVL engine. Therefore, we measure only accuracy in classifying \textit{bitorrent} traffic. We implemented the classifying component that receives SVs and SVM coefficients in the global parameter store to build a model and then use the model to classify flow records of the classifying dataset.  Table II shows the \textit{bitorrent} traffic classifying result.

\begin{table}[h]
\center
\begin{tabular}{l|l|l}
\textbf{Method}           & \textbf{Packets} & \textbf{Bytes} \\ \hline
NAVL engine               & 5810647          & 6257656547     \\ \hline
Proposed Distributed SVMs & 6134168          & 6580016584     \\ 
\end{tabular}
\caption{Bittorent traffic classifying results.}
\end{table}

\input{Korea-app}

We also measured the scalability of the classifying process. Fig. \ref{fig:svmclassifying} shows the scalability of the classifying process in analyzing 101 GB of network traffic. The completion time is reduced as the number of processing nodes increases, but not in a linear function. With 19 Mapper nodes, the proposed system obtains 15 times faster than the standalone solution in classifying 101 GB of network traffic.

\begin{figure}[htp]
\centering
\includegraphics[width=0.45\textwidth]{figures/trainingtime.pdf}
\caption{Scalability of distributed SVM for the training.}
\label{fig:svmtraining}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[width=0.45\textwidth]{figures/classifyingtime.pdf}
\caption{Scalability of distributed SVM for classifying 101 GB network traffic.}
\label{fig:svmclassifying}
\end{figure}


\section{Conclusion}
\subsection{Summary}
In this paper, we have proposed a distributed SVM mechanism for analyzing network traffic. By using Redis in-memory key-value database to store intermediate data, the approach improves scalability, parallelism, and time consumption. The experiments prove that the proposed mechanism could achieve high performance in classifying network traffic data in a distributed way. The proposed architecture is designed with large scale datasets in mind, not only for network traffic data but also other Big Data such as health care data, sensor network data, and WWW data. The proposed distributed SVM mechanism is still under active development to improve its performace, but the current source code is open source and available on Bitbucket\footnote{https://bitbucket.org/lequocdo/DSVM}.
\subsection{Future Work}
First, in large scale deployment, the global parameter store need to be scalable, even though Redis supports cluster version, the Redis developer haven't released stable cluster version. Therefore, Cassandra is considered to replace Redis as a future work, since Cassandra uses the consistent hashing mechanism to distribute data across a cluster to provide the scalability. Moreover, Cassandra version 4.0 supports a new in-memory option that strengthens Cassandra as an in-memory database. Second, selecting good features for learning is very important to improve classifying accuracy, as well as to reduce training time. Therefore, we will investigate more in selecting features for network traffic classification. Finally, implementing other machine learning techniques using the proposed architecture for mining Big Data will also be considered.

\section{Acknowledgement}

This work was supported by the LEADS project, the European funded project with grant agreement number 318809.

\bibliographystyle{plain}
\bibliography{references}

\end{document}